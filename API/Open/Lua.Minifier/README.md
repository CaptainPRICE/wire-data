# Lua.Minifier #

`Lua.Minifier` namespace provides a Lua minifier functionality which allows you to compress Lua source code and minimize file size.