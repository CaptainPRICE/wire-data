# Lua.Decompiler #

`Lua.Decompiler` namespace provides a Lua bytecode decompiler functionality which allows you to obtain a source code of any Lua-defined function.