# Lua.Sandbox #

`Lua.Sandbox` namespace provides a Lua Sandbox functionality which creates a virtual space for each authenticated user and allows them to execute Lua programs under isolated environment.